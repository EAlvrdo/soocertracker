//
//  LabelRegular.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation
import UIKit

class LabelRegular: UILabel {
    // LogoCategoryc class
    var customTextColor: UIColor?
    var size: CGFloat?
    var letterSpacing: CGFloat?
    var lineSpacing: CGFloat?
    var aligment: NSTextAlignment?
    
    init(text: String = "", size: CGFloat = 18.0, textColor: UIColor = UIColor.createColorObject(r: 59, g: 55, b: 75), letterSpacing: CGFloat = 0, lineSpacing: CGFloat = 4, aligment: NSTextAlignment = NSTextAlignment.left, bold: Bool = false){
        super.init(frame: CGRect.zero)
        
        self.isUserInteractionEnabled = true
        
        addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(showMenu)))
        
        self.font = UIFont(name: Constants.View.Fonts.latoRegular, size: size)
        self.textColor = textColor
        self.textAlignment = aligment
        
        if(bold){
            self.font = UIFont.boldSystemFont(ofSize: size)
        }
        
        self.text = text
        
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setAttributedStringText(text: String){
        
        self.text = text
        
    }
    
    //Set text and substring for lenght
    func substringAttributedText(text: String, lenght: Int, lineSpacing: CGFloat = 4){
        
        //addLineSpacing(lineSpacing: lineSpacing)
        
        guard text.count < lenght else {
            
            let index = text.index(text.startIndex, offsetBy: lenght)
            
            self.text = text.substring(to: index) + "..."
            
            return
        }
        
        guard text.count != 0 else {
            return
        }
        
        self.lineSpacing = lineSpacing
        self.text = text
        
    }
    
    func addLineSpacing(lineSpacing: CGFloat = 4){
        
        let attrString = NSMutableAttributedString(string: self.text!)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpacing // change line spacing between paragraph like 36 or 48
        style.minimumLineHeight = lineSpacing // change line spacing between each line like 30 or 40
        attrString.addAttribute(.paragraphStyle, value: style, range: NSRange(location: 0, length: self.text!.count))
        self.attributedText = attrString
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func adjustHeight(){
        self.sizeToFit()
    }
    
    override var canBecomeFirstResponder: Bool { return true }
    
    override func copy(_ sender: Any?) {
        let board = UIPasteboard.general
        board.string = text
    }
    
    @objc func showMenu(sender: AnyObject?) {
        becomeFirstResponder()
        let menu = UIMenuController.shared
        if !menu.isMenuVisible {
            menu.setTargetRect(bounds, in: self)
            menu.setMenuVisible(true, animated: true)
        }
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action  == #selector(UIResponderStandardEditActions.copy(_:)){
            return true
        }else{
            return false
        }
    }
}
