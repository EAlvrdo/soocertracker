//
//  RegularButton.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable

class RegularButton: UIButton {
    
    var primaryBackgroundColor: UIColor?
    var secondaryBackgroundColor: UIColor?
    var originalButtonText: String?
    var activityIndicator: UIActivityIndicatorView!
    
    init(text: String, primaryColor: UIColor = UIColor.c1, secondaryColor: UIColor? = nil, textColor: UIColor = .white, cornerRadius: CGFloat = 30.0){
        // This line is needed to run the super class constructor.
        // The frame is set to zero, because the size of the class
        // will be changed in the future
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        self.primaryBackgroundColor = primaryColor
        if secondaryColor == nil{
            // checkIfprimaryColor is white
            if self.primaryBackgroundColor == UIColor.white{
                self.secondaryBackgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
            }
            else
                if self.primaryBackgroundColor == UIColor.clear{
                    self.secondaryBackgroundColor = UIColor.clear
                }else{
                    self.secondaryBackgroundColor = primaryColor.withAlphaComponent(0.8)
            }
        }else{
            self.secondaryBackgroundColor = secondaryColor!
            
        }
        
        // Set title to the button.
        let font = UIFont(name: Constants.View.Fonts.latoRegular, size: 18)
        self.setAttributedTitle(text.createAttributeString(font: font!, color: textColor), for: .normal)
        self.backgroundColor = primaryColor
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
        
        self.addTarget(self, action: #selector(setSecondaryColor), for: .touchDown)
        self.addTarget(self, action: #selector(setPrimaryColor), for: .touchUpInside)
        
    }
    
    @objc func setPrimaryColor(){
        self.backgroundColor = self.primaryBackgroundColor
        
    }
    
    @objc func setSecondaryColor(){
        self.backgroundColor = self.secondaryBackgroundColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showLoading() {
        originalButtonText = (self.titleLabel?.text)!
        self.setTitle("", for: .normal)
        
        self.backgroundColor = UIColor.pdLightGray
        self.isUserInteractionEnabled = false
        
        if (activityIndicator == nil) {
            activityIndicator = createActivityIndicator()
        }
        
        showSpinning()
    }
    
    func hideLoading() {
        self.setTitle(originalButtonText, for: .normal)
        activityIndicator.stopAnimating()
        
        self.backgroundColor = self.primaryBackgroundColor
        self.isUserInteractionEnabled = true
        
    }
    
    private func createActivityIndicator() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .lightGray
        return activityIndicator
    }
    
    private func showSpinning() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        centerActivityIndicatorInButton()
        activityIndicator.startAnimating()
    }
    
    private func centerActivityIndicatorInButton() {
        let xCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0)
        self.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
        self.addConstraint(yCenterConstraint)
    }
    
    
}
