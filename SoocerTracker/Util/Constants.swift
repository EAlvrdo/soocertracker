//
//  Constants.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation
import UIKit


class Constants {
 
    class View {
        
        // Screen width.
        static public var screenWidth: CGFloat {
            return UIScreen.main.bounds.width
        }
        
        // Screen height.
        static public var screenHeight: CGFloat {
            return UIScreen.main.bounds.height
        }
        
        class Fonts {
            static var latoRegular: String = "Lato-Regular"
        }
        
    }
    
}
