//
//  File.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

/*
 Hi dude, how r u? Here Edwin Alvarado!
 
 Some stuffs that is not configurate, because the time! jeje.
 - Localize strings
 - Insets and offsets with % based on screen size
 - Modularization in firebase response and service
 - Custom navbar
 - Rule with reference in matches with teams. But works perfectly! :)
 
 and other stuffs!
 
 Bye, see you soon!
 Edwin.
 */

import Foundation
import UIKit
import SnapKit
import FirebaseDatabase

class SplashVC: UIViewController{
    
    let mainView: UIView = UIView()
    
    let bgImage: UIImageView = UIImageView(image: UIImage(named: "bg-splash"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLayout()
        self.nextWindow()
        
        //Testing realtime database
        let ref = Database.database().reference()
        
        //Set some value
        //ref.child("Teams/000/name").setValue("Roma")
        
        //Add new value
        //ref.child("Teams").childByAutoId().setValue(["name": "Juventus", "points": "02"])
        
    }
    
    func setupLayout(){
        
        self.view.addSubview(self.mainView)
        self.mainView.snp.makeConstraints { (make) in
            make.width.height.equalToSuperview()
        }
        
        self.mainView.addSubview(self.bgImage)
        self.bgImage.snp.makeConstraints { (make) in
            make.width.height.equalToSuperview()
        }
        self.bgImage.contentMode = .scaleAspectFill
        
        self.view.backgroundColor = .white
        self.hideNavBar(view: self)
        
    }
    
    func nextWindow(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            NotificationCenter.default.removeObserver(self)
            self.navigationController?.pushViewController(MainTabBarVC(), animated: true)
        }
    }
    
}
