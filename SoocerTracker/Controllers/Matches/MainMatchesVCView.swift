//
//  MainMatchesVCView.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
import Firebase
import CodableFirebase

@available(iOS 11.0, *)
class MainMatchesVCView: UITableViewController{
    
    public var matches: [Match] = [Match]()
    public var filteredMatches: [Match] = [Match]()
    
    let searchBar: UISearchController = UISearchController(searchResultsController: nil)
    
    func filterContentForSearchText(searchText: String, scope: String = "All"){
        
        filteredMatches = matches.filter{ match in
            return (match.local.lowercased().contains(searchText.lowercased()) || match.visitor.lowercased().contains(searchText.lowercased()) )
        }
        
        self.tableView.reloadData()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(MatchCell.self, forCellReuseIdentifier: "cellId")
        tableView.isPagingEnabled = true
        tableView.clipsToBounds = true
        
        tableView.contentInsetAdjustmentBehavior = .scrollableAxes
        tableView.contentInset = UIEdgeInsets.zero
        tableView.scrollIndicatorInsets = UIEdgeInsets.zero
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        
        searchBar.searchResultsUpdater = self
        searchBar.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchBar.searchBar
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.getMatches()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        if searchBar.isActive && !searchBar.searchBar.text!.isEmpty {
            return filteredMatches.count
        }
        
        return matches.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.View.screenHeight / 6.2
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.View.screenHeight * 0.005
    }
    
    // Make the background color show through
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let match: Match
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! MatchCell
        
        if searchBar.isActive && !searchBar.searchBar.text!.isEmpty {
            match = filteredMatches[indexPath.section]
        }else{
            match = matches[indexPath.section]
        }
        
        cell.matchTitle.text = "Partido \(indexPath.section)"
        cell.match = match
        
        cell.layoutIfNeeded()
        
        return cell
        
    }
    
    func getMatches(){
        
        let ref = Database.database().reference()
        
        ref.child("Matches").observeSingleEvent(of: .value) { (snapshot) in
            
            guard let value = snapshot.value else { return }
            
            do {
                let matches = try FirebaseDecoder().decode([String: Match].self, from: value)
                
                self.matches.removeAll()
                
                for match in matches{
                    
                    self.matches.append(match.value)
                    self.tableView.reloadData()
                }
            } catch let error {
                print(error)
            }
        }
    }
    
}


extension MainMatchesVCView: UISearchResultsUpdating{
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
    
}
