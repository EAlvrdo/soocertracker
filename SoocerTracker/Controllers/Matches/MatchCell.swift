//
//  MatcheCell.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation
import UIKit

class MatchCell: UITableViewCell {
    
    let cellContainer: UIView = UIView()
    
    var matchTitle: LabelRegular = LabelRegular(text: "",
                                                size: 16,
                                                textColor: UIColor.darkGray,
                                                aligment: .center)
    
    var localImage = UIImageView(image: UIImage(named: "img-team-placeholder"))
    
    var infoContainer: UIStackView = UIStackView()

    var localTitle: LabelRegular = LabelRegular(text: "",
                                                size: 18,
                                                textColor: UIColor.darkGray,
                                                aligment: .left)
    
    var resultsTxt: LabelRegular = LabelRegular(text: "",
                                                size: 18,
                                                textColor: UIColor.darkGray,
                                                aligment: .center)
    
    var visitorTitle: LabelRegular = LabelRegular(text: "",
                                                size: 18,
                                                textColor: UIColor.darkGray,
                                                aligment: .right)
    
    let visitorImage = UIImageView(image: UIImage(named: "img-team-placeholder"))
    
    var match: Match? {
        didSet{
            guard let unwrappedMatch = match else {return}
            
            localTitle.text = match?.local
            localTitle.sizeToFit()

            var resultAppendText: String = (match?.localGoals)!
            resultAppendText += " - "
            resultAppendText += (match?.visitorGoals)!
            
            resultsTxt.text = resultAppendText
                
            
            visitorTitle.text = match?.visitor
            visitorTitle.sizeToFit()
            
            //matchImage.image = get image from server
            
            self.layoutIfNeeded()
            
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLayout(){
        
        self.addSubview(cellContainer)
        self.cellContainer.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.95)
            make.height.equalToSuperview().multipliedBy(0.90)
            make.center.equalToSuperview()
        }
        self.cellContainer.backgroundColor = .white
        
        self.cellContainer.layer.cornerRadius = 8
        self.cellContainer.layer.shadowColor = UIColor.black.cgColor
        self.cellContainer.layer.shadowOpacity = 0.5
        self.cellContainer.layer.shadowOffset = CGSize.zero
        self.cellContainer.layer.shadowRadius = 3
        
        self.cellContainer.addSubview(self.matchTitle)
        self.matchTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview()
            make.width.equalToSuperview()
        }
        self.matchTitle.sizeToFit()
        self.matchTitle.numberOfLines = 1
        
        self.cellContainer.addSubview(localImage)
        self.localImage.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.50)
            make.width.equalToSuperview().multipliedBy(0.10)
            make.left.equalToSuperview().offset(Constants.View.screenWidth * 0.025)
        }
        self.localImage.contentMode = .scaleAspectFill
        
        self.infoContainer = UIStackView(arrangedSubviews: [localTitle, resultsTxt, visitorTitle])
        
        self.cellContainer.addSubview(self.infoContainer)
        self.infoContainer.snp.makeConstraints { (make) in
            make.left.equalTo(localImage.snp.right).offset(Constants.View.screenWidth * 0.035)
            make.height.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.70)
            make.centerY.equalToSuperview()
        }
        self.infoContainer.distribution = .fillEqually
        
        self.infoContainer.translatesAutoresizingMaskIntoConstraints = false
        
        self.cellContainer.addSubview(visitorImage)
        self.visitorImage.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.50)
            make.width.equalToSuperview().multipliedBy(0.10)
            make.right.equalToSuperview().inset(Constants.View.screenWidth * 0.025)
        }
        self.visitorImage.contentMode = .scaleAspectFill

        
    }
    
}
