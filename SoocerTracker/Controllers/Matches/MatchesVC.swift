//
//  MatchesVC.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase

class MatchesVC: UIViewController{
    
    let header: LabelRegular = LabelRegular(text: "SOOCER TRACKER",
                                            size: 22,
                                            textColor: UIColor.white,
                                            aligment: .center)
    
    let mainTableController: MainMatchesVCView = MainMatchesVCView()
    
    lazy var mainViewTransaction: UIView = self.mainTableController.view
    
    let addButton: ButtonRegular = ButtonRegular(text: "",
                                                 primaryColor: UIColor.clear,
                                                 textColor: UIColor.clear,
                                                 cornerRadius: 25)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLayout()
        self.setupMainViewActions()
        self.hideNavBar(view: self)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.mainTableController.viewDidAppear(animated)
    }
    
    func setupLayout(){
        
        self.view.addSubview(self.header)
        self.header.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.10)
            make.top.equalToSuperview()
        }
        self.header.backgroundColor = UIColor.darkGray
        
        self.view.addSubview(self.mainViewTransaction)
        self.mainViewTransaction.snp.makeConstraints { (make) in
            make.center.width.equalTo(self.view)
            make.height.equalToSuperview().multipliedBy(0.85)
            make.top.equalTo(self.header.snp.bottom)
        }
        
        self.view.addSubview(self.addButton)
        self.addButton.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.18)
            make.height.equalToSuperview().multipliedBy(0.10)
            make.right.equalToSuperview().inset(15)
            make.bottom.equalToSuperview().inset(70)
        }
        
        self.addButton.setImage(UIImage(named: "ic-btn-floating"), for: .normal)
        self.addButton.layer.cornerRadius = ((Constants.View.screenHeight * 0.10) / 2)
        
        self.addButton.imageView?.contentMode = .scaleAspectFit
        self.addButton.contentMode = .scaleAspectFit
        self.addButton.imageEdgeInsets = UIEdgeInsets.zero
        self.addButton.insetsLayoutMarginsFromSafeArea = false
        self.addButton.imageView?.layoutIfNeeded()
        
    }
    
    func setupMainViewActions(){
        
        self.addButton.addTarget(self, action: #selector(addMatch), for: .touchUpInside)
        
    }
    
    @objc func addMatch(){
        
        let alertController = UIAlertController(title: "Add Match", message: "", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            
            let localName = alertController.textFields![0] as UITextField
            let localGoals = alertController.textFields![1] as UITextField
            let visitorName = alertController.textFields![2] as UITextField
            let visitorGoals = alertController.textFields![3] as UITextField
            
            self.saveMatch(localName: localName.text!, localGoals: localGoals.text!, visitorName: visitorName.text!, visitorGoals: visitorGoals.text!)
            
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Local name"
        })
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Local goals"
            textField.keyboardType = .numberPad
        })
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Visitor name"
        })
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Visitor goals"
            textField.keyboardType = .numberPad
        })

        self.present(alertController, animated: true, completion: nil)
        
    }

    func saveMatch(localName: String, localGoals: String, visitorName: String, visitorGoals: String){
        
        //Show in list
        let newMatch: Match = Match(local: localName,
                                    localGoals: localGoals,
                                    visitor: visitorName,
                                    visitorGoals: visitorGoals)
        
        self.mainTableController.matches.append(newMatch)
        self.mainTableController.tableView.reloadData()
        
        //Save in Firebase
        let ref = Database.database().reference()
        
        ref.child("Matches").childByAutoId().setValue(["local": localName,
                                                       "local-goals": localGoals,
                                                       "visitor": visitorName,
                                                       "visitor-goals": visitorGoals])
    }
}
