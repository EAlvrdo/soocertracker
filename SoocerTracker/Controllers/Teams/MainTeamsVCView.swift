//
//  MainTeamsVCView.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation
import UIKit
import CodableFirebase
import FirebaseDatabase

@available(iOS 11.0, *)
class MainTeamsVCView: UITableViewController{
    
    public var teams: [Team] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(TeamCell.self, forCellReuseIdentifier: "cellId")
        tableView.isPagingEnabled = true
        tableView.clipsToBounds = true
        
        tableView.contentInsetAdjustmentBehavior = .scrollableAxes
        tableView.contentInset = UIEdgeInsets.zero
        tableView.scrollIndicatorInsets = UIEdgeInsets.zero
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getTeams()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return teams.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.View.screenHeight / 6.2
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.View.screenHeight * 0.005
    }
    
    // Make the background color show through
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! TeamCell
        
        let team = teams[indexPath.section]
        cell.team = team
        
        cell.layoutIfNeeded()
        
        return cell
        
    }
    
    func getTeams(){
        
        let ref = Database.database().reference()
        
        ref.child("Teams").observeSingleEvent(of: .value) { (snapshot) in
            
            guard let value = snapshot.value else { return }
            
            do {
                let teams = try FirebaseDecoder().decode([String: Team].self, from: value)
                
                for team in teams{
                    
                    if (!self.teams.contains(where: { $0.name == team.value.name})){
                        self.teams.append(team.value)
                    }
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
                    self.tableView.reloadData()
                }
                
            } catch let error {
                print(error)
            }
        }
    }
    
}
