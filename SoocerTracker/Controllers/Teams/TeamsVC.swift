//
//  TeamsVC.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
import CodableFirebase
import Firebase

@available(iOS 11.0, *)
class TeamsVC: UIViewController{
    
    let header: LabelRegular = LabelRegular(text: "SOOCER TRACKER",
                                            size: 22,
                                            textColor: UIColor.white,
                                            aligment: .center)
    
    let mainTableController: MainTeamsVCView = MainTeamsVCView()
    
    lazy var mainViewTransaction: UIView = self.mainTableController.view
    
    let addButton: ButtonRegular = ButtonRegular(text: "",
                                                      primaryColor: UIColor.clear,
                                                      textColor: UIColor.clear,
                                                      cornerRadius: 25)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLayout()
        self.setupMainViewActions()
        self.hideNavBar(view: self)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.mainTableController.viewDidAppear(animated)
    }
    
    func setupLayout(){
        
        self.view.addSubview(self.header)
        self.header.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.10)
            make.top.equalToSuperview()
        }
        self.header.backgroundColor = UIColor.darkGray
        
        self.view.addSubview(self.mainViewTransaction)
        self.mainViewTransaction.snp.makeConstraints { (make) in
            make.center.width.equalTo(self.view)
            make.height.equalToSuperview().multipliedBy(0.85)
            make.top.equalTo(self.header.snp.bottom)
        }
        
        self.view.addSubview(self.addButton)
        self.addButton.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.18)
            make.height.equalToSuperview().multipliedBy(0.10)
            make.right.equalToSuperview().inset(15)
            make.bottom.equalToSuperview().inset(70)
        }
        
        self.addButton.setImage(UIImage(named: "ic-btn-floating"), for: .normal)
        self.addButton.layer.cornerRadius = ((Constants.View.screenHeight * 0.10) / 2)
        
        self.addButton.imageView?.contentMode = .scaleAspectFit
        self.addButton.contentMode = .scaleAspectFit
        self.addButton.imageEdgeInsets = UIEdgeInsets.zero
        self.addButton.insetsLayoutMarginsFromSafeArea = false
        self.addButton.imageView?.layoutIfNeeded()
        
    }
    
    func setupMainViewActions(){
        
        self.addButton.addTarget(self, action: #selector(addTeam), for: .touchUpInside)
        
    }
    
    @objc func addTeam(){
        
        let alertController = UIAlertController(title: "Add Team", message: "", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            
            let teamName = alertController.textFields![0] as UITextField
            let teamPoints = alertController.textFields![1] as UITextField
            
            self.saveTeam(teamName: teamName.text!, teamPoints: teamPoints.text!)
            
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Team name"
        })
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Team points"
            textField.keyboardType = .numberPad
        })
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func saveTeam(teamName: String, teamPoints: String){
        
        guard (!self.mainTableController.teams.contains(where: { $0.name == teamName})) else {
            
            Util.showMessage(message: "This team already exist!")
            return
        }
        //Show in list
        let newTeam: Team = Team(name: teamName,
                                 points: teamPoints)
        
        self.mainTableController.teams.append(newTeam)
        self.mainTableController.tableView.reloadData()
        
        let ref = Database.database().reference()
        
        ref.child("Teams").childByAutoId().setValue(["name": teamName,
                                                       "points": teamPoints])
    }
    
}
