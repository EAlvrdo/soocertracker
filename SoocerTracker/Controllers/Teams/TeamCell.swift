//
//  TeamCell.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation
import UIKit

class TeamCell: UITableViewCell {
    
    let cellContainer: UIView = UIView()
    
    let teamLocalImage = UIImageView(image: UIImage(named: "img-team-placeholder"))
    
    let infoContainer: UIView = UIView()
    
    let teamTitle: LabelRegular = LabelRegular(text: "",
                                               size: 18,
                                               textColor: UIColor.darkGray,
                                               aligment: .left,
                                               bold: true)
    
    var team: Team? {
        didSet{
            guard let unwrappedTeam = team else {return}
            
            teamTitle.text = team?.name
            
            //matchImage.image = get image from server with kingfisher and cache options
            
            self.layoutIfNeeded()
            
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLayout(){
        
        self.addSubview(cellContainer)
        self.cellContainer.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.95)
            make.height.equalToSuperview().multipliedBy(0.90)
            make.center.equalToSuperview()
        }
        self.cellContainer.backgroundColor = .white
        
        self.cellContainer.layer.cornerRadius = 8
        self.cellContainer.layer.shadowColor = UIColor.black.cgColor
        self.cellContainer.layer.shadowOpacity = 0.5
        self.cellContainer.layer.shadowOffset = CGSize.zero
        self.cellContainer.layer.shadowRadius = 3
        
        self.cellContainer.addSubview(teamLocalImage)
        self.teamLocalImage.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.75)
            make.width.equalToSuperview().multipliedBy(0.15)
            make.left.equalToSuperview().offset(Constants.View.screenWidth * 0.025)
        }
        self.teamLocalImage.contentMode = .scaleAspectFill
        
        self.cellContainer.addSubview(self.infoContainer)
        self.infoContainer.snp.makeConstraints { (make) in
            make.left.equalTo(teamLocalImage.snp.right).offset(Constants.View.screenWidth * 0.035)
            make.height.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.67)
            make.centerY.equalToSuperview()
        }
        
        self.infoContainer.addSubview(teamTitle)
        self.teamTitle.snp.makeConstraints { (make) in
            make.left.equalTo(infoContainer.snp.left)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.95)
        }
        self.teamTitle.adjustHeight()
        self.teamTitle.numberOfLines = 1
        
        
        
    }
    
}
