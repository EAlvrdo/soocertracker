//
//  MainTabBarVC.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class MainTabBarVC: UITabBarController, UITabBarControllerDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.hideNavBar(view: self)
        self.setupLayout()
    }
    
    func setupLayout(){
        
        
        self.view.backgroundColor = UIColor.white
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.createTabBarItems()
        
        self.createNavBar()
        
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func createTabBarItems(){
        
        // Create tab teams
        let teamsVC = self.createControllerAndItem(viewController: TeamsVC(), imageNormal: UIImage(named: "ic-team")!, imageSelected: UIImage(named: "ic-team-selected")!, title: "EQUIPOS")
        
        // Create tab matches
        let matchesVC = self.createControllerAndItem(viewController: MatchesVC(),imageNormal: UIImage(named: "ic-match")!, imageSelected: UIImage(named: "ic-match-selected")!, title: "PARTIDOS")
        
        self.viewControllers = [teamsVC, matchesVC]
        
        // Taggin the viewcontrollers for the tab controller
        for index in 0...(self.viewControllers?.count)! - 1 {
            self.viewControllers?[index].tabBarItem.tag = index
        }
        
        //Adding selector color
        let numberOfItems = CGFloat(tabBar.items!.count)
        let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height)
        
        tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor.lightGray, size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)
        
    }
    
    func createControllerAndItem(viewController: UIViewController, imageNormal: UIImage, imageSelected: UIImage, title: String) -> UIViewController {
        
        // Create Tab item
        let tabBarItem = UITabBarItem(title: ""/*title*/, image: imageNormal.withRenderingMode(.alwaysOriginal), selectedImage: imageSelected.withRenderingMode(.alwaysOriginal))
        
        viewController.tabBarItem = tabBarItem
        viewController.tabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        return viewController
    }
    
    func createNavBar(){
        
        self.navigationItem.title = "Socker Tracker - Champions"
    }
    
}

