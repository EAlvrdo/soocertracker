//
//  UIViewController+Utilities.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/20/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    func hideNavBar(view: UIViewController){
        view.navigationController?.isNavigationBarHidden = true
    }
    
}
