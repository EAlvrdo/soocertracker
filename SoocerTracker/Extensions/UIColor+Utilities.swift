//
//  UIColor+Utilities.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    class var darkGray: UIColor {
        return UIColor(red: 175.0 / 255.0, green: 188.0 / 255.0, blue: 196.0 / 255.0, alpha: 1.0)
    }
    
    class var lightGray: UIColor {
        return UIColor(red: 216.0 / 255.0, green: 222.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0)
    }
    
    static func createColorObject(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat = 1) -> UIColor{
        
        let constant : CGFloat = 255
        
        return UIColor(red: r / constant, green: g / constant, blue: b / constant, alpha: 1.0)
    }
    
}
