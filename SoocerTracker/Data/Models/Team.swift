//
//  Team.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation

struct Team: Codable{
    
    let name: String
    let points: String
    
}
