//
//  Match.swift
//  SoocerTracker
//
//  Created by Edwin Alvarado on 12/19/18.
//  Copyright © 2018 Interfell. All rights reserved.
//

import Foundation

struct Match: Codable{
    
    let local: String
    let localGoals: String
    let visitor: String
    let visitorGoals: String
    
    enum CodingKeys: String, CodingKey{
        case local
        case localGoals = "local-goals"
        case visitor
        case visitorGoals = "visitor-goals"
    }
    
}
